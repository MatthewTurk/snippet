def show_command(cmd, c_args=None):
    str = "{cmd} {c_args}".format(
        cmd=cmd,
        c_args=c_args,
        )
    print(str)


def show_details(snippet):
    str = '\n'.join([
        "id          : {}".format(snippet.id),
        "is_private  : {}".format(snippet.is_private),
        "is_unlisted : {}".format(snippet.is_unlisted),
        "title       : {}".format(snippet.title),
        "files       : {}".format(snippet.filenames),
        "creator     : {} <{}>".format(
            snippet.creator['display_name'],
            snippet.creator['username']),
        "created_on  : {}".format(snippet.created_on),
        "owner       : {} <{}>".format(
            snippet.owner['display_name'],
            snippet.owner['username']),
        "updated_on  : {}".format(snippet.updated_on),
        "scm         : {}".format(snippet.scm),
        ]) if snippet else 'Snippet not found.'
    print(str)


def show_line(snippet):
    private = '-' if snippet.is_private else '+'
    unlisted = 'u' if snippet.is_unlisted else 'l'
    title = snippet.title if snippet.title else '<untitled>'
    str = "{id} {private}{unlisted} {title} {files}".format(
        id=snippet.id,
        private=private,
        unlisted=unlisted,
        title=title,
        files=snippet.filenames,
        )
    print(str)


def show_filename(f):
    print(f)


def show_content(c):
    print(c)


def show_user(u):
    str = "{} <{}>".format(u['display_name'], u['username'])
    print(str)


def show_comment(c):
    str = """Author: {display_name} <{username}>
Date:   {created_on}

    {content}
""".format(
        display_name=c['user']['display_name'],
        username=c['user']['username'],
        created_on=c['created_on'],
        content=c['content']['raw'],
        )
    print(str)


def show_commit(c):
    print(c)


def show_server_error(e):
    str = """Oops! Bitbucket seems to be having a problem.
For more information check the Bitbucket status page:
http://status.bitbucket.org/

{}""".format(e.message)
    print(str)
