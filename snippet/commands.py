from getpass import getpass
from utils import get_first
from utils import is_private
from utils import is_unlisted

from pybitbucket.snippet import open_files
from pybitbucket.snippet import Snippet

import display


def fail(args):
    display.show_command('fail')


def auth(args):
    username = raw_input('Username: ')
    password = getpass()
    display.show_command('auth', '%s:%s' % (username, password))


def list(args):
    role_switch = get_first(
        [k for (k, v) in args.iteritems()
         if (v and (k in ['--owner', '--contributor', '--member']))])
    role = role_switch.translate(None, '-') if role_switch else 'owner'
    display.show_command('list', 'role=%s' % role)
    for snip in Snippet.find_snippets_for_role(role):
        display.show_line(snip)


def create(args):
    params = {
        'title': args['--title'],
        'is_private': is_private(args['--public'], args['--private']),
        'is_unlisted': is_unlisted(args['--listed'], args['--unlisted']),
        'files': args['FILES'],
    }
    display.show_command('create', params)
    snip = Snippet.create_snippet(
        files=open_files(params['files']),
        title=params['title'],
        is_private=params['is_private'],
        is_unlisted=params['is_unlisted'])
    display.show_details(snip)


def modify(args):
    params = {
        'id': args['<id>'],
        'title': args['--title'],
        'is_private': is_private(args['--public'], args['--private']),
        'is_unlisted': is_unlisted(args['--listed'], args['--unlisted']),
        'files': args['FILES'],
    }
    display.show_command('modify', params)
    snip = Snippet.find_snippet_by_id(params['id'])
    snip.modify(
        files=open_files(params['files']),
        title=params['title'],
        is_private=params['is_private'],
        is_unlisted=params['is_unlisted'])
    display.show_details(snip)


def delete(args):
    id = args['<id>']
    display.show_command('delete', 'id=%s' % id)
    snip = Snippet.find_snippet_by_id(id)
    display.show_line(snip)
    if snip:
        snip.delete()


def info(args):
    id = args['<id>']
    display.show_command('info', 'id=%s' % id)
    snip = Snippet.find_snippet_by_id(id)
    display.show_details(snip)


def files(args):
    id = args['<id>']
    display.show_command('files', 'id=%s' % id)
    snip = Snippet.find_snippet_by_id(id)
    for file in snip.files:
        display.show_filename(file)


def content(args):
    id = args['<id>']
    filename = args['<filename>']
    display.show_command('content', 'id=%s filename=%s' % (id, filename))
    snip = Snippet.find_snippet_by_id(id)
    c = snip.content(filename)
    display.show_content(c)


def watchers(args):
    id = args['<id>']
    display.show_command('watchers', 'id=%s' % id)
    snip = Snippet.find_snippet_by_id(id)
    for watcher in snip.watchers():
        display.show_user(watcher)


def comments(args):
    id = args['<id>']
    display.show_command('comments', 'id=%s' % id)
    snip = Snippet.find_snippet_by_id(id)
    for comment in snip.comments():
        display.show_comment(comment)


def commits(args):
    id = args['<id>']
    display.show_command('commits', 'id=%s' % id)
    snip = Snippet.find_snippet_by_id(id)
    for commit in snip.commits():
        display.show_commit(commit)


def commit(args):
    id = args['<id>']
    sha1 = args['<sha1>']
    display.show_command('commit', 'id=%s sha1=%s' % (id, sha1))
