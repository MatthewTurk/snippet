#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
A command-line interface for creating, inspecting, and editing
Bitbucket Snippets.

usage:
    snippet (-h | --help)
    snippet auth [--user=<username>]
    snippet list [--owner|--contributor|--member]
    snippet create [--title=<title>] [--public|--private]
                   [--listed|--unlisted] [FILES ...]
    snippet modify <id> [--title=<title>] [--public|--private]
                   [--listed|--unlisted] [FILES ...]
    snippet delete <id>
    snippet info <id>
    snippet files <id>
    snippet content <id> <filename>
    snippet watchers <id>
    snippet comments <id>
    snippet commits <id>
    snippet commit <id> <sha1>
    snippet --version
"""
from __future__ import print_function
import sys
from docopt import docopt
import metadata
import commands
from utils import get_first
from display import show_server_error

from pybitbucket.bitbucket import ServerError


def is_command(s):
    if s.startswith('-'):
        return False
    if s.startswith('<'):
        return False
    return True


def dispatch_command(arguments, command='fail'):
    f = getattr(commands, "%s" % command, commands.fail)
    return f(arguments)


def main(argv):
    version_string = '{0} {1}'.format(metadata.project, metadata.version)
    arguments = docopt(__doc__, argv=argv, version=version_string)
    command = get_first([k for (k, v) in arguments.iteritems()
                         if (v and is_command(k))])
    try:
        dispatch_command(arguments, command)
    except ServerError, e:
        show_server_error(e)


def entry_point():
    #"""Zero-argument entry point for use with setuptools/distribute."""
    main(sys.argv[1:])

if __name__ == '__main__':
    entry_point()
