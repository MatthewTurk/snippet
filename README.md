# Snippet

A command-line interface for creating, inspecting, and editing 
Bitbucket Snippets.

## Developing

### Python Virtual Environment Setup (for OS X)

It's not virtual like a virtual machine. More like a specialized "container" for a Python version and libraries.

1. `brew install python` This installs the latest version of Python 2.7 with a version of setuptools and pip. Unfortunately, those versions of setuptools and pip seem to be broken.
2. `pip install --upgrade --no-use-wheel setuptools`
3. `pip install --upgrade --no-use-wheel pip`
4. `pip install virtualenvwrapper`

### Project Setup

1. Clone the repository and set it as the current working directory.
2. *(Optional, but good practice)* Create a [virtual environment](http://docs.python-guide.org/en/latest/dev/virtualenvs/): `mkvirtualenv snippet` Once created, use `workon snippet` to restore the virtual environment.
3. `pip install -r requirements-dev.txt` Loads required libraries into the virtual environment.
4. `pip install git+https://ian_buchanan@bitbucket.org/ian_buchanan/python-bitbucket.git@v0.3.0#egg=pybitbucket` Loads the not-yet-released pybitbucket library to the environment using the last known stable tag.
5. `paver test_all` Run all the unit tests (there aren't many) and analyze the source code.
6. `python snippet/main.py --help`

To pull cutting edge changes from pybitbucket:
```
pip install git+https://ian_buchanan@bitbucket.org/ian_buchanan/python-bitbucket.git#egg=pybitbucket --upgrade
```

## TODO

* When skipping public and listed switches, `modify` still sets public and listed attributes.
* Implement `auth` to create and modify the configuration.
* Find out why `commits` throws a server error.